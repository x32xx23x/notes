My notes from elastic engineering course

# Module 2: You know, for search
## Lesson 1: Search options
- ElasticSQL: "Gives access to all Elasticsearch options"
## Lesson 2: Searching with the Query DSL
- `GET index_name/_search`, can be comma separated list or wildcards
- Contains two parts: query and aggregations
# Module 3: Data modeling
- Fields that will not be used for search can be set `"index": false` in the mapping
- Fields that will not be used at all can be set `"enable": false` in the mapping
- copy_to in the mapping to add several fields to a single field search. [link](https://www.elastic.co/guide/en/elasticsearch/reference/current/copy-to.html)
- dynamic_templates to map fields by rules
- Runtime fields only calculated on query
# Module 4: Data processing
- `_update_by_query` update indices without re indexing, for example when adding a multifield
- transforms
# Module 5: The one about shards
- shards: instances of lucene, primary or replica. Index is partitioned in shards
- A shard can usually hold tenth of gbs. Do not overshard!

# Module 6: Data management
- Searchable snapshots

